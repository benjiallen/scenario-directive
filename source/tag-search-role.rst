Tag search role
###############

Test 1
======

On it's own line.

:tag_search:`tag_a`

Test 2
======

In running text :tag_search:`tag_a`
