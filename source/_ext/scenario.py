from docutils import nodes, utils
from docutils.parsers.rst import Directive, directives, roles

from sphinx.roles import XRefRole
from urllib.parse import quote

"""
Inspiration
===========

/Users/coder/anaconda3/envs/scenario-directive/lib/python3.6/site-packages/docutils/parsers/rst/directives/body.py
LineBlock directive
CodeBlock directive

Good to know
============

/Users/coder/anaconda3/envs/scenario-directive/lib/python3.6/site-packages/docutils/parsers/rst/states.py
RSTState class
Line 423, def inline_text(self, text, lineno):

/Users/coder/anaconda3/envs/scenario-directive/lib/python3.6/site-packages/sphinx/roles.py
How Sphinx does linking

/Users/coder/anaconda3/envs/scenario-directive/lib/python3.6/site-packages/sphinx/domains/std.py
How Sphinx resolves links, also good to review how a domain is written

/Users/coder/anaconda3/envs/scenario-directive/lib/python3.6/site-packages/sphinx/builders/__init__.py
Home of get_relative_uri
"""

class Scenario(Directive):
    def list_of_strings(a):
        # taken from ablog post.py
        return [s.strip() for s in (a or "").split(",") if s.strip()]
    def true_or_false(argument):
        try:
            value = argument.lower().strip()
        except AttributeError:
            raise ValueError('must supply an argument; choose from true or false')
        if value in ('true', 'false'):
            if value == 'true':
                return True
            else:
                return False
        else:
            raise ValueError('"%s" unknown; choose from true or false'
                             % argument)

    # remember an option is different to an argument
    # argument comes right after the directive.
    # TODO: add the ability to add the scenario title as an argument
    # optional_arguments = 1
    option_spec = {'title': directives.unchanged,
                   'tags': list_of_strings,
                   'indent': true_or_false}
    has_content = True

    def run(self):
        self.assert_has_content()
        node_list = []
        if 'tags' in self.options:
            tags = self.options['tags']
            para_node = nodes.paragraph(rawsource='',
                                        text='',
                                        classes=['scenario-tag'])
            role_fn = _custom_roles['tag_search']
            for t in tags:
                para_node += nodes.inline(rawsource='@' + t,
                                          text='@' + t,
                                          classes=['scenario-tag-inline'])
                para_node += nodes.Text(', ', rawsource=', ')
            # remove the last comma from the tag list
            para_node.pop()
            node_list.append(para_node)
        # TODO: need to play around with this to see if I can get a class passed
        block = nodes.line_block(classes=self.options.get('class', ['scenario-block']))
        # TODO: need to investigate what this does
        self.add_name(block)
        node_list.append(block)
        indent = 0
        if self.options.get('indent', True):
            indent = 1
        # if the indent is True then line.indent of the scenario title has to be less than
        # the line.indent of the scenario contents lines
        # if the indent is False then line.indent of the scenario title has to be the same
        # as the line.indent of the scenario contents lines
        if 'title' in self.options:
            # TODO: needs refactoring, could just add this to self.content
            # this is a copy and paste of the code below.
            title = self.options['title']
            text_nodes, messages = self.state.inline_text(
                                                '**Scenario:** ' + title.strip(),
                                                self.lineno + self.content_offset)
            line = nodes.line(title, '', *text_nodes, classes=['scenario-title'])
            if title.strip():
                line.indent = 0
            block += line
            node_list.extend(messages)
        for line_text in self.content:
            # TODO:
            # need to investigate refactoring and just making a line block node
            # by calling the right class. Maybe pass in the scenario title as a child?
            # TODO:
            # confused by this line, need to research
            # not sure why self.state is used
            # unclear what inline_text does (guess: creates Text nodes)
            # confused further by messages variable
            text_nodes, messages = self.state.inline_text(
                line_text.strip(), self.lineno + self.content_offset)
            # TODO: try adding a text_node with white space to the text_nodes list
            # TODO: is the <pre> element a block level element? 
            # create some line nodes with the text passed
            # test_white_space = nodes.literal_block(rawsource='  ', text='  ')
            line = nodes.line(line_text, '', *text_nodes)
            if line_text.strip():
                line.indent = indent
            # TODO: should check out where this gets overloaded
            # clearly this is adding the line to the line block
            # but it would interesting to see the data structure
            block += line
            # TODO: why add messages to the node_list and not block?
            # add messages (no idea what that is) to the node_list
            # https://docs.python.org/3/tutorial/datastructures.html?highlight=built%20data%20structures#more-on-lists
            node_list.extend(messages)
            # TODO: need to research what content_offset does
            self.content_offset += 1
        # TODO:
        # 1. need to research why state is used here
        # 2. need to understand why nest_line_block_lines is called
        # this is where more parsing into a doctree happens
        self.state.nest_line_block_lines(block)
        # for n in node_list:
        #     print(n.pformat())
        return node_list

class TagSearch(XRefRole):
    def process_link(self, env, refnode, has_explicit_title, title, target):
        # type: (BuildEnvironment, nodes.Element, bool, str, str) -> Tuple[str, str]
        """Called after parsing title and target text, and creating the
        reference node (given in *refnode*).  This method can alter the
        reference node and must return a new (or the same) ``(title, target)``
        tuple.
        """
        refuri = env.app.builder.get_relative_uri(env.docname, 'search')
        # use of the @ symbol comes from the gherkin spec for tags
        refnode.attributes['refuri'] = refuri + '?q=' + quote('@' + utils.unescape(title))
        # TODO: add an aria-label to the tag link. Might require a custom node.
        return '@' + title, target

# creating an instance of the class and placing it in a dict so I can reference later
# Strictly speaking, this does not need to be in a dict. This will make more sense
# when creating more than 1 role.
_custom_roles = {'tag_search': TagSearch(nodeclass=nodes.reference,
                                         innernodeclass=nodes.inline)}

def setup(app):
    app.add_directive("scenario", Scenario)
    app.add_role('tag_search', _custom_roles['tag_search'])

    return {
        'version': '0.2',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
