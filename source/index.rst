.. Sphinx customizations documentation master file, created by
   sphinx-quickstart on Sun Jan 12 20:03:11 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx customizations's documentation!
=================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   scenario
   scenario-tag-a
   scenario-in-folder-to-test-search-links/scenario-tag-b
   tag-search-role
   tag-in-folder-to-test-search-links/tag-search-role-in-folder
   dev-notes
