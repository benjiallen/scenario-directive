########################################
Scenario with tag b (nested folder test)
########################################

.. scenario::
   :title: A user can search for tag b
   :tags: tag_b

   Given I have a scenario
   And the scenario has a tag_b
   When I click on the tag
   Then the search results will include this page
