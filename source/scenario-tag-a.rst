###################
Scenario with tag a
###################

.. scenario::
   :title: A user can search for tag a
   :tags: tag_a

   Given I have a scenario
   And the scenario has a tag_a
   When I click on the tag
   Then the search results will include this page
