Scenario
########

Test 1
======

Scenario directive with content. Content has no RST within it.

.. code-block:: rst

   .. scenario::

      Given something
      When something
      Then something

Some text before

.. scenario::

   Given test 1
   When test 1
   Then test 1

Some text after

Test 2
======

Scenario directive with content. Content has RST within it. In this case, bold and a link.

.. code-block:: rst

   .. scenario::

      **Given** something
      **When** something
      **Then** something `Deque <https://www.deque.com/>`_

Some text before

.. scenario::

   **Given** test 2
   **When** test 2
   **Then** test 2 `Deque <https://www.deque.com/>`_

Some text after

Test 3
======

Scenario directive with ``title`` option.

.. code-block:: rst

   .. scenario::
      :title: scenario title

      Given something
      When something
      Then something

.. scenario::
   :title: scenario title

   Given test 3
   When test 3
   Then test 3

Test 4
======

Scenario directive with ``title`` and tags option.

.. code-block:: rst

   .. scenario::
      :title: scenario title
      :tags: tag_a, tag_b, tag_c

      Given something
      When something
      Then something

.. scenario::
   :title: scenario title
   :tags: tag_a, tag_b, tag_c

   Given test 4
   When test 4
   Then test 4

Test 5
======

Scenario directive with ``indent`` option.

.. code-block:: rst

   .. scenario::
      :title: scenario title
      :tags: tag_a, tag_b, tag_c
      :indent: True

      Given something
      When something
      Then something

.. scenario::
   :title: scenario title
   :tags: tag_a, tag_b, tag_c
   :indent: True

   Given test 5
   When test 5
   Then test 5

Test 6
======

Scenario directive with ``indent`` option and indent already in place.

.. code-block:: rst

   .. scenario::
      :title: scenario title
      :tags: tag_a, tag_b, tag_c
      :indent: True

        Given something
        When something
        Then something

.. scenario::
   :title: scenario title
   :tags: tag_a, tag_b, tag_c
   :indent: True

     Given test 6
     When test 6
     Then test 6

Test 7
======

Scenario directive with ``indent`` option set to False.

.. code-block:: rst

   .. scenario::
      :title: scenario title
      :tags: tag_a, tag_b, tag_c
      :indent: False

      Given something
      When something
      Then something

.. scenario::
   :title: scenario title
   :tags: tag_a, tag_b, tag_c
   :indent: False

   Given test 7
   When test 7
   Then test 7

Exception cases
===============

Test 1
------

Scenario directive with no content.

This will throw an exception during the build and will not output any HTML.

.. code-block:: rst

   .. scenario::
      :title: no content example
      :tags: exception, no-content

.. scenario::
      :title: no content example
      :tags: exception, no-content

Test 2
------

Scenario directive with ``indent`` option set to an unknown option.

This will throw an exception during the build and will not output any HTML.

.. code-block:: rst

   .. scenario::
      :title: scenario title
      :tags: tag_a, tag_b, tag_c
      :indent: bad value

      Given something
      When something
      Then something

.. scenario::
      :title: scenario title
      :tags: tag_a, tag_b, tag_c
      :indent: bad value

      Given something
      When something
      Then something

Experimenets with linking to a search  page
===========================================

Quick start
-----------

The way that sphinx-quickstart links to start.

I can't find a way to pass a query string.

.. code-block:: rst

   :ref:`search`

:ref:`search`

External link method
--------------------

Pretty sure this method is brittle. It uses a relative link which will likely break or put burden on the author.

To make this more solid, I could figure out the document root within the python code and then build the link programatically.

.. code-block:: rst

   `Search Ben <./search.html?q=ben>`_

`Search Ben <./search.html?q=ben>`_

Failed attempts
---------------

.. code-block:: rst

   :doc:`search`
   :ref:`search#query`
   :ref:`search?query`

Experimenets with pygments
==========================

.. code-block:: gherkin

   Scenario: this is a scenario

   Given I am here
   When I did this
   Then this happens
