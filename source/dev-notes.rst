Useful notes - creating my first extension
##########################################

User setup
==========

1. Create a ``_ext`` folder within the ``source`` folder
2. Register the extension within ``conf.py``

Nomenclature
============

Document Tree
-------------

`The Docutils Document Tree`_ seems to be an important concept. My understanding is as follows:

* docutils builds a document tree ("doctree") represented in XML

  - Building a document tree in this way makes sense because the final document format is variable i.e., could be HTML, could be a man page etc.

* The python implementation of the "nodes" represented in the doctree are captured in the nodes module
* When building a directive, you need to be thinking about the docutils doctree and not the HTML DOM
* `Sphinx build phases <https://www.sphinx-doc.org/en/master/extdev/index.html#build-phases>`_ support my understanding of the doctree and specifically calls out the importance in separating reading from writing
* Sadly `The Docutils Document Tree`_ documentation is not complete so it makes it hard to work out which doctree nodes map to HTML nodes e.g., which node represents a link (HTML ``<a href="">``)?

  - The most reliable method to fill in the gaps seems to be finding a directive or role, in the docutils source code, which is doing something similar to the output you want to receive.

Debugging the doctree
~~~~~~~~~~~~~~~~~~~~~

docutils method
+++++++++++++++

It looks like docutils comes with `useful tools <https://docutils.sourceforge.io/docs/user/tools.html#xml-generating-tools>`_ to inspect the doctree.

I need to experiment with registering my directive with docutils and inspecting the output of the doctree with the tools. Once I get the doctree right, taking it over to sphinx should be easy.

It also looks like docutils can be passed a `config file <https://docutils.sourceforge.io/docs/user/config.html#dump-pseudo-xml>`_  and there are lots of debug options in there.

sphinx method
+++++++++++++

`Debugging tips <https://www.sphinx-doc.org/en/master/devguide.html#debugging-tips>`_ suggest using the following:

  Use ``node.pformat()`` and ``node.asdom().toxml()`` to generate a printable representation of the document structure.

Directives
----------

Directives are indicated by an explicit markup start (".. ") followed by the directive type, two colons, and whitespace (together called the "directive marker").

The directive block is consists of any text on the first line of the directive after the directive marker, and any subsequent indented text. The interpretation of the directive block is up to the directive code. There are three logical parts to the directive block:

#. Directive arguments.
#. Directive options.
#. Directive content.

Individual directives can employ any combination of these parts. Directive arguments can be filesystem paths, URLs, title text, etc. Directive options are indicated using field lists; the field names and contents are directive-specific. Arguments and options must form a contiguous block beginning on the first or second line of the directive; a blank line indicates the beginning of the directive content block. If either arguments and/or options are employed by the directive, a blank line must separate them from the directive content. The "figure" directive employs all three parts:

.. code-block:: rst

   .. figure:: larch.png
      :scale: 50

      The larch.

Roles
-----

Usage of custom role:

.. code-block:: rst

   :tag_search:`tag-name`

* `Creating reStructuredText Interpreted Text Roles <https://docutils.sourceforge.io/docs/howto/rst-roles.html>`_
* `Registering the role with Sphinx "add_role" <https://www.sphinx-doc.org/en/master/extdev/appapi.html#sphinx.application.Sphinx.add_role>`_

Whitespace
==========

Findings so far:

* There is a `fixedspace` attribute within the DTD

  - The base node which uses this attribute is called `FixedTextElement`
  - The base node is the super class of a few nodes, including the `literal_block`

* The `literal_block` node (nodes.py) seems to be the node used by the `code-block` directive (body.py)
* You can simulate white space within a `line_block` and `line` node by playing with `line.indent`

  - This will not create real white space. It will use CSS left padding. This is problematic from a "copy and paste use case" perspective because the padding is not copied and some formatting will be lost.

* Is this a case where it makes sense to create a custom node? An inline node that preserves white space?
* The `<pre>` tag, the tag which preserves whitespace in HTML, does let you add `<strong>` and `<em>` tags to it's contents

Thinking process
================

* Get the directive registered and outputing something without content
* Get the content working - each line should have it's own node
* Customize the content with inline elements like bold (``<strong>``) or italics (``<em>``)
* Generate links to search - this is harder than I thought

  - No easy way to pass the search page a query
  - Could do it the hard way and build the link to the search page in python

    + Would need to find the root of the build and then find search from there
    + Then do some string mangling to add a query string to the search URL

  - Long term, it seems that the best way to do it would be to create some form of index

    + ablog probably has the best/most similar solution in that it creates quick look ups for tags

Useful links
============

.. _`The Docutils Document Tree`: https://docutils.sourceforge.io/docs/ref/doctree.html#inline-elements

* `The Docutils Document Tree`_
* `Creating reStructuredText Directives <https://docutils.sourceforge.io/docs/howto/rst-directives.html>`_
* `Understanding the Directive nomenclature <https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html#directives>`_
* `Functions that the themes and templates use <https://www.sphinx-doc.org/en/master/templating.html#helper-functions>`_
